#ifndef _STRING_H_
#define _STRING_H_

#include <stdint.h>

typedef struct {
	char* data;
	int length;
} string_t;

string_t string_create(char* s);
void string_destroy(string_t* string);

string_t string_copy(string_t* string, int length);

int string_is_valid(string_t* string);

int string_index_of(string_t* string, char c);
int string_last_index_of(string_t* string, char c);

int string_trim_whitespace(string_t* string);

#endif // _STRING_H_
