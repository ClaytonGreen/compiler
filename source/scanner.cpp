// Author: Clayton Green (kgreen1@unbc.ca)

#include <iostream>
#include <stdlib.h>

#include "administrator.h"
#include "scanner.h"
#include "messenger.h"

#if 0
Scanner::Scanner(const std::string& built_in_functions_source, Messenger* messenger)
	: filename_("built-in"), done_(false), messenger(messenger) {
	source_ = built_in_functions_source;
	current_char_ = source_.begin();
	line_number_ = 1;
	PrintLine();
}
#endif

Scanner::Scanner(string_t* source, Messenger* messenger)
    : source(source),
      done_(false),
      messenger(messenger)
{
    //current_char_ = ;
    line_number_ = 1;
    PrintLine();
}

Token
Scanner::GetToken()
{
    if (done_)
    {
        return Token(Token::ENDFILE);
    }

    // Check to see if there is white space
    while (IsWhitespace(PeekChar()))
    {
        if (PeekChar() == '\n')
        {
            GetChar();
            ++line_number_;
            PrintLine();
        }
        else
        {
            GetChar();
        }
    }

    char peek_char = PeekChar();
    do
    {
        if (IsLetter(peek_char))
        {
            // Then it starts with a letter
            // It's an ID or another keyword
            std::string lexeme;
            lexeme += GetChar();
            peek_char = PeekChar();
            if (IsLetter(peek_char) || IsDigit(peek_char) || peek_char == '_' || peek_char == '$')
            {
                while (IsLetter(peek_char) || IsDigit(peek_char) || peek_char == '_' || peek_char == '$')
                {
                    lexeme += GetChar();
                    peek_char = PeekChar();
                }
            }
            return Token(Token::ID, lexeme);
        }
        else if (IsDigit(peek_char))
        {
            // Then it's a number
            std::string number;
            number += GetChar();
            peek_char = PeekChar();
            while (IsDigit(peek_char))
            {
                number += GetChar();
                peek_char = PeekChar();
            }
            return Token(Token::NUM, static_cast<int>(atof(number.c_str())));
        }
        switch (peek_char) {
            case '+':
                GetChar(); // eat it!
                return Token(Token::PLUS);

            case '-':
                GetChar(); // eat it again!
                if (PeekChar() == '-')
                {
                    // Read in the rest of the line
                    while (PeekChar() != '\n')
                    {
                        GetChar();
                        if (done_)
                        {
                            break;
                        }
                    }
                    return GetToken();
                }
                return Token(Token::MINUS);

            case '*':
                GetChar(); // eat it as well!
                return Token(Token::MULT);

            case '/':
                GetChar(); // eat it also!
                if (PeekChar() == '=')
                {
                    GetChar();
                    return Token(Token::NEQ);
                }
                else if (PeekChar() == '*')
                {
                    GetChar();
                    if (EatComment() == -1)
                    {
                        return Token(Token::ENDFILE);
                    }
                    else
                    {
                        return GetToken();
                    }
                }
                return Token(Token::DIV);

            case '&':
                GetChar();
                if (PeekChar() == '&')
                {
                    GetChar();
                    return Token(Token::ANDTHEN);
                }

                // TODO: How do I access Administrator's messenger
                messenger->AddError(filename_, line_number_, "Invalid Token '&'");
                return Token(Token::ERROR);

            case '|':
                GetChar();
                if (PeekChar() == '|')
                {
                    GetChar();
                    return Token(Token::ORELSE);
                }

                // TODO: How do I access Administrator's messenger
                messenger->AddError(filename_, line_number_, "Invalid Token '|'");
                return Token(Token::ERROR);

            case '<':
                GetChar();
                if (PeekChar() == '=')
                {
                    GetChar();
                    return Token(Token::LTEQ);
                }
                return Token(Token::LT);
                break;

            case '>':
                GetChar();
                if (PeekChar() == '=')
                {
                    GetChar();
                    return Token(Token::GTEQ);
                }
                return Token(Token::GT);

            case '=':
                GetChar();
                return Token(Token::EQ);

            case ':':
                GetChar();
                if (PeekChar() == '=')
                {
                    GetChar();
                    return Token(Token::ASSIGN);
                }
                return Token(Token::COLON);

            case ';':
                GetChar();
                return Token(Token::SEMI);

            case ',':
                GetChar();
                return Token(Token::COMMA);

            case '(':
                GetChar();
                return Token(Token::LPAREN);

            case ')':
                GetChar();
                return Token(Token::RPAREN);

            case '[':
                GetChar();
                return Token(Token::LSQR);

            case ']':
                GetChar();
                return Token(Token::RSQR);

            case '{':
                GetChar();
                return Token(Token::LCRLY);

            case '}':
                GetChar();
                return Token(Token::RCRLY);

            case '_':
            case '$':
            {
                std::string temp;
                temp += GetChar();
                while (!IsWhitespace(PeekChar()))
                {
                    temp += GetChar();
                }

                std::string message = "Invalid Token '";
                message += temp;
                message += "'";
                messenger->AddError(filename_, line_number_,
                message);
                return Token(Token::ERROR);
                break;
            }
            default:
                // Check if we're at the end
                if (!done_)
                {
                    char temp = GetChar();
                    std::string message = "Invalid Token '";
                    message += temp;
                    message += "'";
                    messenger->AddError(filename_, line_number_, message);

                    return Token(Token::ERROR);
                }
                return Token(Token::ENDFILE);
        }
    } while (true);

    std::string message = "An unexpected error has occurred.";
    messenger->AddError(filename_, line_number_, message);
    return Token(Token::ERROR);
}

int
Scanner::EatComment() 
{
    int count = 1;
    int start_comment_line = line_number_;
    bool plus = false;
    bool minus = false;
    bool in_comment = true;
    while (count > 0)
    {
        if (done_)
        {
            std::string message = "Reached EOF before comment was closed.";
            messenger->AddError(filename_, start_comment_line,
            message);
            return -1;
        }
        else if (PeekChar() == '\n')
        {
            GetChar();
            ++line_number_;
            PrintLine();
        }
        else if (in_comment)
        {
            if (PeekChar() == '/') {
                GetChar();
                plus = true;
                minus = false;
                in_comment = false;
            }
            else if (PeekChar() == '*')
            {
                GetChar();
                minus = true;
                plus = false;
                in_comment = false;
            }
            else 
            {
                GetChar();
            }
        }
        else if (plus)
        {
            if (PeekChar() == '*')
            {
                GetChar();
                ++count;
                plus = false;
                minus = false;
                in_comment = true;
            }
            else
            {
                plus = false;
                minus = false;
                in_comment = true;
            }
        }
        else if (minus) 
        {
            if (PeekChar() == '/')
            {
                GetChar();
                count--;
                minus = false;
                plus = false;
                in_comment = true;
            }
            else
            {
                minus = false;
                plus = false;
                in_comment = true;
            }
        }
    }
    return 0;
}

bool
Scanner::IsControlCode(const char& character) const
{
    // A space and newline and carrage return don't count as control characters
    if (IsWhitespace(character))
    {
        return false;
    }
    return (
            (int)character >= 0 && 
            (int)character < 32 ||
            (int)character == 127
    );
}

bool
Scanner::IsDigit(const char& character) const
{
    return (character >= '0' && character <= '9');
}

bool
Scanner::IsLetter(const char& character) const
{
    return (character >= 'A' && character <= 'Z' ||
            character >= 'a' && character <= 'z');
}

bool
Scanner::IsWhitespace(const char& character) const
{
    return ((int)character == 9 ||  // TAB
            (int)character == 10 || // LF
            (int)character == 13 || // CR
            (int)character == 32);  // SPACE
}

char
Scanner::PeekChar()
{
    if (source->length <= 0)
    {
        return -1;
    }

    int offset = 0;
    char c = source->data[offset];
    while ((source->length - offset) > 0)
    {
        if (!IsControlCode(c))
        {
            break;
        }
        c = source->data[++offset];
    }

    return source->data[offset];
}

char
Scanner::GetChar()
{
    if (source->length <= 0)
    {
        done_ = true;
        return -1;
    }

    char c = *source->data;
    source->data++;
    source->length--;

    if (source->length == 0)
    {
        done_ = true;
    }
    
    return c;
}

void
Scanner::PrintLine()
{
    int end = 0;
    for (int i = 0; i < source->length && source->data[i] != '\n'; i++)
    {    
        end++;
    }

    string_t line = string_copy(source, end);
    string_trim_whitespace(&line);

    char buffer[1024];
    string_t message;
    message.data = buffer;
    message.length = sprintf(buffer, "%.*s\n", line.length, line.data);
    messenger_print(messenger, &message);

    string_destroy(&line);
}
