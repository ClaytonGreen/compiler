
#include <stdlib.h>
#include <string.h>

#include "strings.h"

string_t
string_create(char* s)
{
    string_t result;
    result.length = strlen(s) + 1;
    result.data = (char*)calloc(result.length, sizeof(*result.data));
    if (result.data != 0)
    {
        memcpy(result.data, s, result.length * sizeof(*result.data));
    }
    else
    {
        result.length = 0;
    }
    return result;
}

void
string_destroy(string_t* string)
{
    if (string->data != 0)
    {
        free(string->data);
    }
    string->length = 0;
    string->data = 0;
}

string_t
string_copy(string_t* string, int length)
{
    string_t result;

    if (length > string->length)
    {
        result.length = string->length;
    }
    else
    {
        result.length = length;
    }

    result.data = (char*)calloc(result.length, sizeof(*result.data));
    if (result.data == 0)
    {
        result.length = 0;
    }
    else
    {
        memcpy(result.data, string->data, result.length * sizeof(*result.data));
    }

    return result;
}

int string_is_valid(string_t* string)
{
    if (string->data == 0) {
        return 1;
    }
    return 0;
}

int string_index_of(string_t* string, char c)
{
    for (int i = 0; i < string->length; i++)
    {
        if (string->data[i] == c)
        {
            return i;
        }
    }
    return -1;
}

int string_last_index_of(string_t* string, char c)
{
    for (int i = (string->length - 1); i >= 0; i--)
    {
        if (string->data[i] == c)
        {
            return i;
        }
    }
    return -1;
}

static int
is_whitespace(char c)
{
    if (c == ' ' || c == '\t' || c == '\r' || c == '\n') {
        return 1;
    }
    return 0;
}

int string_trim_whitespace(string_t* string)
{
    int start = 0;
    for ( ; start < string->length; start++)
    {
        char c = string->data[start];
        if (is_whitespace(c) == 0)  break;
    }

    int end = (string->length - 1);
    for ( ; end >= 0; end--)
    {
        char c = string->data[end];
        if (!is_whitespace(c))  break;
    }

    if (start > 0 || end < (string->length - 1))
    {
        // save pointer so we can free later
        char* old_data = string->data;

        int length = end - start + 1;

        size_t size = length * sizeof(*string->data); 
        void* new_data = malloc(size);
        if (new_data == 0)
        {
            return 1; // TODO: actual error codes?
        }

        memcpy(new_data, &string->data[start], size);
        string->length = length;
        string->data = (char*)new_data;

        free(old_data);
    }

    return 0; 
}
