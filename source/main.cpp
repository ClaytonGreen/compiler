// Clayton Green (kgreen1@unbc.ca)
// Last Updated: October 12, 2013

#include <algorithm>

#include <stdio.h>

#include "strings.h"
#include "administrator.h"

int main(int argc, char* argv[])
{
    // TODO: peace out early if there are not enough arguments
    // TODO: handle long arg names. ex: --verbose, --lexer

    config_t config = { 0 };
    for (int i = 1; i < argc; i++)
    {
        char* arg = argv[i];
        if (arg[0] == '-')
        {
            if (strcmp(arg, "-l") == 0)
            {
                config.lex = true;
                continue;
            }
            else if (strcmp(arg, "-p") == 0)
            {
                config.parse = true;
                continue;
            }
            else if (strcmp(arg, "-s") == 0)
            {
                config.sem = true;
                continue;
            }
            else if (strcmp(arg, "-t") == 0)
            {
                config.tup = true;
                continue;
            } 
            else if (strcmp(arg, "-q") == 0)
            {
                config.quiet = true;
                continue;
            }
            else if (strcmp(arg, "-v") == 0)
            {
                config.verbose = true;
                continue;
            } 
            else if (strcmp(arg, "-o") == 0)
            {
                // TODO: handle output source
            } 
            else if (strcmp(arg, "-e") == 0)
            {
                // TODO: handle error output source
            }
        }
        else
        {
            config.num_files = (argc - i);
            config.files = (string_t*)calloc(config.num_files, sizeof(*config.files));
            if (config.files == 0)
            {
                fprintf(stderr, "Failed to malloc input file list");
                return -1;
            }
            for (int j = 0; j < config.num_files; j++)
            {
                char* file = argv[i++];
                config.files[j] = string_create(file);
            }
        }
    }

    if (config.num_files == 0) {
        fprintf(stderr, "Compiles .c13 files\n\nkompiler files\n\n    files\tList of files to compile\n\nVERSION 0.1\n");
        return -1;
    }

    //std::string out = out_arg.getValue();
    //if (out.compare("out") != 0) {
    //	// TODO: Set output file
    //}

    Administrator admin(&config);
    //if (err.compare("err") != 0) {
    //	// TODO: USE FILE HANDLES
    //	// Have to create the error file to be able to write to it
    //	std::ofstream filestream(err_arg.getValue());
    //	if (!filestream) {
    //		fprintf(stderr, "Error creating %s\n", err_arg.getValue().c_str());
    //		return -1;
    //	}
    //	filestream.clear();
    //	filestream.close();
    //	admin = new Administrator(inputFiles, err_arg.getValue());
    //}

    // Lexer Phase: Convert those characters into tokens
    if (config.lex)
    {
        admin.LexerPhase();
    }
    else if (config.parse)
    {
        // Parse Phase: Maybe get some AST?
        admin.ParserPhase();
    }
    else if (config.sem)
    {
        // Semantic Analysis Phase: Type Check and things
        admin.SemanticAnalysisPhase();
    }
    else if (config.tup)
    {
        return -1;
    }
    else
    {
        admin.Compile();
    }

    return 0;
}
