// Author: Clayton Green (kgreen1@unbc.ca)

#include <iostream>
#include <map>

#include <stdio.h>

#include "administrator.h"
#include "ast_node.h"
#include "ast_node_print_visitor.h"
#include "messenger.h"
#include "semantic_analyzer.h"

const std::string Administrator::kBuiltInFunctions = "int readint(void) {return 0;}\nvoid writeint(int outint) {;}\nbool readbool(void) {return true;}\nvoid writebool(bool outbool) {;}\n";

Administrator::Administrator(config_t* config)
    : num_files(config->num_files),
      files(config->files),
      messenger_(config->verbose), 
      show_trace_(config->verbose) 
{
}

// TODO: Add ability to set error file again
// Administrator::Administrator(const std::vector<std::string> file_list, const std::string& error_file)
// 	: input_file_list_(file_list), 
//           messenger_(error_file), 
//           show_trace_(false) 
// {
// }

static string_t
get_filename_from_path(string_t* string)
{
    string_t result;
    result.length = 0;
    result.data = 0;

    // TODO: platform independent
    int index = string_last_index_of(string, '\\');
    if (index != -1 && (index + 1) < string->length)
    {
        index++; // skip the SLASH
        result.length = string->length - index;
        result.data   = (string->data + index);
    }
    else
    {
        result.length = string->length;
        result.data   = string->data;
    }

    return result;
}

static string_t
load_file(string_t* file)
{
    string_t result;
    result.length = 0;
    result.data = 0;

    FILE* fp = fopen(file->data, "rb");
    if (!fp)
    {
        // TODO: better errors, propogate errors
        fprintf(stderr, "Error opening file: %.*s\n", file->length, file->data);
        return result;
    }

    fseek(fp, 0, SEEK_END);
    result.length = (size_t)ftell(fp);
    fseek(fp, 0, SEEK_SET);
    result.data = (char*)malloc(result.length);
    if (result.data == 0)
    {
        // TODO: print to stderr???
        printf("Failed to allocate memory for file: %.*s\n", file->length, file->data);
        return result;
    }
    fread(result.data, result.length, 1, fp);
    fclose(fp);

    return result;
}

static int
tokenize(Messenger* messenger, string_t* source)
{
    int result = 0;

    Scanner scanner(source, messenger);

    char message_data[256];
    string_t message;
    message.length = 0;
    message.data = message_data;
    
    Token token;
    for (token = scanner.GetToken(); token.name() != Token::ENDFILE; token = scanner.GetToken())
    {
        if (token.name() == Token::ERROR) result = 1;

        message.length = sprintf(message.data, "  %d> %s\n", scanner.line_number(), token.toString().c_str());
        if (message.length < 0) {
            // TODO: error
            result = 2;
            printf("ERROR\n");
            continue;
        }

        messenger_print(messenger, &message);
    }

    if (result != 0)
    {
        messenger->PrintErrors();
    }
    else if (token.name() == Token::ENDFILE)
    {
        message.length = sprintf(message.data, "  %d> %s\n", scanner.line_number(), token.toString().c_str());
        messenger_print(messenger, &message);
    }

    return result;
}

static void
parse()
{

}

static void
analyze()
{

}


bool Administrator::Compile()
{
    bool success = true;
    for (int i = 0; i < num_files; i++)
    {
        string_t file = files[i];
        printf("  %.*s\n", file.length, file.data);
        string_t filename = get_filename_from_path(&file);
        if (filename.length == 0)
        {
            messenger_.PrintMessage("Failed to get filename from path\n");
            success = false;
            continue;
        }

        string_t source = load_file(&file);
        int result = string_trim_whitespace(&source);
        if (result != 0 || string_is_valid(&source) != 0)
        {
            printf("Failed to trim whitespace\n");
            success = false;
            continue;  
        }

        // printf("================================================================================\n");
        // printf("%.*s\n", source.length, source.data);
        // printf("================================================================================\n");

        result = tokenize(&messenger_, &source);
        if (result != 0)
        {
            printf("Failed to lex\n");
            success = false;
            continue;
        }

        parse();
        analyze();
    }


    // TODO: Finish all compilation stages
    return success;
}

Administrator::SpellingTable Administrator::CreateSpellingTable() {
	SpellingTable table;
	return table;
}

Administrator::WordMap Administrator::CreateWordMap() {
	Administrator::WordMap map;
	// Add all of the reserved words that have no values (if, and, or, etc.)
	// Entries are of the form: (lexeme, (TokenName, value)).
	for (int i = 0; i < Token::ID; ++i) {
		auto value = std::make_pair(Token::TokenName(i), -1);
		auto pair = std::make_pair(reserved_words_list[i], value);
		map.insert(pair);
	}
	// Add true and false with their respective values, 1 and 0
	auto true_token = std::make_pair("true", std::make_pair(Token::BLIT, 1));
	auto false_token = std::make_pair("false", std::make_pair(Token::BLIT, 0));
	map.insert(true_token);
	map.insert(false_token);

	return map;
}

int Administrator::GetSpellingTableIndex(const std::string & key) {
#pragma warning(disable:4018)  // Unsigned int compared to signed int
	for (int index = 0; index < spelling_table.size(); ++index) {
		if (spelling_table[index] == key) {
			return index;
		}
	}
#pragma warning(default:4018)
	return -1;
}

Administrator::WordMap::const_iterator Administrator::GetWord(
	const std::string & key) {
	auto it = word_map.find(key);
	if (it == word_map.end()) {
		// If the key wasn't found, put it on the spelling table
		spelling_table.push_back(key);
		auto value = std::make_pair(Token::ID, num_spelling_table_entries);
		auto pair = std::make_pair(key, value);
		word_map.insert(pair);
		last_spelling_table_entry = num_spelling_table_entries++;
		return word_map.find(key);
	}
	return it;
}

std::string Administrator::IntToString(const int& num) {
	// Have to use static_cast<long long> because of a bug with gcc-4.3.4
	return std::to_string(static_cast<long long>(num));
}

bool Administrator::LexerPhase()
{
    printf("Lexing...\n");
    bool error_free = true;
#if 0
    for (int i = 0; i < num_files; i++)
    {
        string_t file = files[i];
        printf("  %.*s\n", file.length, file.data);
        string_t filename = get_filename_from_path(&file);
        if (filename.length == 0) {
            messenger_.PrintMessage("Failed to get filename from path\n");
            continue;
		}
		else {
			printf("  %.*s\n", filename.length, filename.data);
		}

        // TODO: why file and filename??
        Scanner scanner(std::string((char*)file.data), std::string((char*)filename.data), &messenger_);
        if (!scanner.good()) {
            fprintf(stdout, "Error opening %.*s\n", filename.length, filename.data);
            continue;
        }

        Token token = scanner.GetToken();
        std::string line_number, message;
        while (token.name() != Token::ENDFILE) {
            line_number = IntToString(scanner.line_number());
            message = std::string();
            message += line_number + "> " + token.toString() + "\n";
            messenger_.PrintMessage(message);
            token = scanner.GetToken();
            if (token.name() == Token::ERROR) error_free = false;
        }
        // Print the ENDFILE token
        message = std::string();
        message += line_number + "> " + token.toString() + "\n";
        messenger_.PrintMessage(message);
        // ---
        messenger_.PrintErrors();
    }
#endif
    return error_free;
}

bool Administrator::ParserPhase() {
#if 0
    printf("Parsing...\n");
    for (int i = 0; i < num_files; i++)
    {
        string_t file = files[i];
        printf("  %.*s\n", file.length, file.data);
        string_t filename = get_filename_from_path(&file);
        if (filename.length == 0) {
            messenger_.PrintMessage("Failed to get filename from path\n");
            continue;
        }

        Parser parser(std::string((char*)file.data), std::string((char*)filename.data), this);
        if (!parser.good()) {
            fprintf(stderr, "Error parsing %.*s\n", filename.length, filename.data);
            continue;
        } else {
            ASTNode* root = parser.Parse();
            if (root && show_trace_) {
                messenger_.PrintMessage("\nAbstract Syntax Tree\n");
                root->Accept(new ASTNodePrintVisitor(&messenger_));
            }
        }
        messenger_.PrintErrors();
    }
#endif
    return true;
}

bool Administrator::SemanticAnalysisPhase()
{
#if 0
    printf("Analyzing...\n");
    // Run the parser to generate an AST
    for (int i = 0; i < num_files; i++)
    {
        string_t file = files[i];
        printf("  %.*s\n", file.length, file.data);
        string_t filename = get_filename_from_path(&file);
        if (filename.length == 0) {
            messenger_.PrintMessage("Failed to get filename from path\n");
            continue;
        }

        // Get just the filename, not the whole path
        Parser parser(std::string((char*)file.data), std::string((char*)filename.data), this);
        if (!parser.good())
        {
            fprintf(stderr, "Error parsing file: %.*s\n", filename.length, filename.data);
            continue;
        }
        // TODO: Why the hell do we "compile" built in functions every time? Can we pre-build the AST?
        // Get the AST for the built in functions 
        // disable trace
        Parser p(kBuiltInFunctions, this);
        ASTNode* new_root = p.Parse();
        ASTNode* root = parser.Parse();
        if (!root) 
        {
            // If the root node is NULL then the parser encountered errors. Print
            // those errors and continue on to the next source file.
            messenger_.PrintErrors();
            continue;
        }
        else 
        {
            // Append the original root not to the end of the new_root
            // Get to the last declaration in new_root
            DeclarationNode* node = dynamic_cast<ProgramNode*>(new_root)->declaration_node();
            while (node->next_node()) 
            {
                node = dynamic_cast<DeclarationNode*>(node->next_node());
            }
            DeclarationNode* next = dynamic_cast<ProgramNode*>(root)->declaration_node();
            node->set_next_node(next);
            // Begin semantic analysis
            SemanticAnalyzer sem(new_root, std::string((char*)filename.data), this);
            sem.InitTraversal();
            sem.FullTraversal();
        }
        messenger_.PrintErrors();
    }
#endif
    return true;
}

bool Administrator::TupleGenerationPhase() {
    // TODO: implement tuple generation
    return false;
}

int Administrator::last_spelling_table_entry = 0;

int Administrator::num_spelling_table_entries = 0;

std::string Administrator::reserved_words_list[] = {
    "and",
    "bool",
    "branch",
    "case",
    "continue",
    "default",
    "else",
    "end",
    "exit",
    "if",
    "int",
    "loop",
    "mod",
    "not",
    "or",
    "ref",
    "return",
    "void",
};

Administrator::SpellingTable Administrator::spelling_table = Administrator::CreateSpellingTable();
Administrator::WordMap Administrator::word_map = Administrator::CreateWordMap();
